import { onMount } from "svelte";
import { writable } from "svelte/store";
import { amp, browser, dev, mode, prerendering } from "$app/env";
import toast from "../components/toast";
import responseHandler from "./utils/fetchResponseHandler";

/** List the tasks
 
 Permissions: `userTask_write`
 @param query Takes `startDate` and `endDate` as query parameters
 */
async function get(query: string) {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "GET",
    headers: [["Content-Type", "application/json"]],
  };

  let result = await await fetch(
    `${import.meta.env.VITE_API_URL}/userTask/` + query,
    requestOptions
  );

  return await responseHandler(result);
}

/** Create a new user task
 * 
 *Permissions: `userTask_write`
 @param user_id User that the task is assigned to
 @param user_id_dep Deputy user that the task is assigned to
 @param date Date of the task
 @param task_id The task id
 @param created_by UID of the user that created the task
 */
async function create(data) {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "POST",
    headers: [["Content-Type", "application/json"]],
    body: JSON.stringify(data),
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/userTask/`,
    requestOptions
  );

  return await responseHandler(result, true);
}

/** Change the status of a user task 
 @param task_id The task id
*/
async function changeStatus(task_id: number, reload = true) {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "PATCH",
    headers: [["Content-Type", "application/json"]],
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/userTask/${task_id}/changeStatus`,
    requestOptions
  );
  return await responseHandler(result, reload);
}
/** Delete a task

 Permissions: `userTask_write`
 @param task_id The task id
 */
async function deleteTask(task_id: number) {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "DELETE",
    headers: [["Content-Type", "application/json"]],
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/userTask/${task_id}/`,
    requestOptions
  );
  return await responseHandler(result, false, "Erfolgreich gelöscht.");
}

/** Get the userTasks that weren't done

 Permissions: `user.write` or own `uid`
 @param user_id The user id
 */
async function getUndoneTasks(task_id: number) {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    headers: [["Content-Type", "application/json"]],
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/userTask/undoneTasks/${task_id}/`,
    requestOptions
  );
  return await responseHandler(result);
}

export default {
  get,
  create,
  changeStatus,
  deleteTask,
  getUndoneTasks,
};
