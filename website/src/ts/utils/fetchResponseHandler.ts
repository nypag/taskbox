import { browser } from "$app/env";
import toast from "../../components/toast";

const responseHandler = async (
  response: Response,
  reload?: boolean,
  text?: string
) => {
  if (response.ok) {
    if (text != undefined) {
      toast.success(text);
    }
    if (reload) window.location.reload();
    return await response.json();
  } else if (response.status == 401) {
    if (browser && window.location.pathname != "/user/signIn")
      window.location.replace("/user/signIn");
  } else if (response.status == 403) {
    history.back();
    toast.error(await response.text());
  } else {
    toast.error(await response.text());
  }
};

export default responseHandler;
