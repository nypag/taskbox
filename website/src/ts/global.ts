function dateFormatter(dateTime) {
  var date = new Date(Date.parse(dateTime));
  return (
    ("0" + date.getDate()).slice(-2) +
    "." +
    ("0" + (date.getMonth() + 1)).slice(-2) +
    "." +
    date.getFullYear()
  );
}

export function dateToTime(date: Date) {
  return (
    ("0" + date.getHours()).slice(-2) +
    ":" +
    ("0" + date.getMinutes()).slice(-2)
  );
}

export function timeToDate(time) {
  return new Date("1970-01-01T" + time + ":00.000Z");
}
export default {
  dateFormatter,
};
