import { onMount } from "svelte";
import { writable } from "svelte/store";
import { amp, browser, dev, mode, prerendering } from "$app/env";
import toast from "../components/toast";
import { timeToDate } from "./global";
import responseHandler from "./utils/fetchResponseHandler";

// Get the session token from local storage
async function get(query = "") {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "GET",
    headers: [["Content-Type", "application/json"]],
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/task/${query}`,
    requestOptions
  );
  return await responseHandler(result);
}

async function update(taskObject, task_id) {
  taskObject.deadline_time = timeToDate(taskObject.deadline_time);
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "PATCH",
    headers: [["Content-Type", "application/json"]],
    body: JSON.stringify(taskObject),
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/task/${task_id}`,
    requestOptions
  );
  return await responseHandler(result, true);
}

async function create(taskObject) {
  taskObject.deadline_time = timeToDate(taskObject.deadline_time);
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "POST",
    headers: [["Content-Type", "application/json"]],

    body: JSON.stringify(taskObject),
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/task`,
    requestOptions
  );
  return await responseHandler(result, true);
}

async function deleteTask(task_id) {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "DELETE",
    headers: [["Content-Type", "application/json"]],
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/task/${task_id}`,
    requestOptions
  );
  return await responseHandler(result, true);
}

export default {
  get,
  update,
  deleteTask,
  create,
};
