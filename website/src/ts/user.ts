import { onMount } from "svelte";
import { writable } from "svelte/store";
import { amp, browser, dev, mode, prerendering } from "$app/env";
import toast from "../components/toast";
import responseHandler from "./utils/fetchResponseHandler";
import jwtDecode from "jwt-decode";
import Cookies from "js-cookie";

/** get the user */
async function get() {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "GET",
    headers: [["Content-Type", "application/json"]],
  };
  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/user`,
    requestOptions
  );

  return await responseHandler(result);
}

/** list all users */
async function list(showHiddenUsers = false) {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "GET",
    headers: [["Content-Type", "application/json"]],
  };

  let result = await fetch(
    `${
      import.meta.env.VITE_API_URL
    }/user/list?showHiddenUsers=${showHiddenUsers}`,
    requestOptions
  );

  return await responseHandler(result);
}

/** Change the password of the user
 @param oldPassword the old password
 @param newPassword the new password
 */
async function changePassword(oldPassword: string, newPassword: string) {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "POST",
    headers: [["Content-Type", "application/json"]],
    body: JSON.stringify({
      oldPassword: oldPassword,
      newPassword: newPassword,
    }),
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/user/changePassword`,
    requestOptions
  );

  return await responseHandler(result, false, "Passwort geändert.");
}

/** Update a user

 Permissions: `user_write`
 @param userObject The object with the new user data
 @param user_id The user_id of the user
 */
async function updateUser(userObject, user_id: number) {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "PATCH",
    headers: [["Content-Type", "application/json"]],
    body: JSON.stringify(userObject),
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/user/${user_id}`,
    requestOptions
  );

  return await responseHandler(
    result,
    false,
    "Der Benutzer wurde aktualisiert."
  );
}

/** Create a new user
 * 
 Permissions: `user_write`
 @param userObject The object with the new user data
 */
async function signUp(user) {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "POST",
    headers: [["Content-Type", "application/json"]],
    body: JSON.stringify({
      email: user.email.toLowerCase(),
      first_name: user.first_name,
      last_name: user.last_name,
      password: user.password,
      roleId: user.roleId,
    }),
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/user/signUp`,
    requestOptions
  );

  return await responseHandler(
    result,
    false,
    "Die Registrierung war erfolgreich."
  );
}

/** Reset the password of an user
 
 Permissions: `user_write`
 */
async function resetPassword(newPassword: string, user_id: number) {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "POST",
    headers: [["Content-Type", "application/json"]],
    body: JSON.stringify({
      user_id: user_id,
      newPassword: newPassword,
    }),
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/user/resetPassword`,
    requestOptions
  );

  return await responseHandler(result, false, "Das Passwort wurde geändert.");
}

/** Delete a user
 
 Permissions: `user_write`
 @param user_id The user_id of the user
 */
async function deleteUser(user_id: number) {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "DELETE",
    headers: [["Content-Type", "application/json"]],
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/user/${user_id}`,
    requestOptions
  );
  return await responseHandler(result, true);
}

/** Delete the session */
async function signOut() {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "POST",
    headers: [["Content-Type", "application/json"]],
  };
  await fetch(`${import.meta.env.VITE_API_URL}/user/signOut`, requestOptions);
  localStorage.clear();
  sessionStorage.clear();
  window.location.reload();
}

/** List all roles
  
 Permissions: `user.read`
 */
async function listRoles() {
  var requestOptions = {
    credentials: "include" as RequestCredentials,
    method: "GET",
    headers: [["Content-Type", "application/json"]],
  };

  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/role`,
    requestOptions
  );

  return await responseHandler(result);
}

function checkIfSignedIn(httpResponse: Response) {
  if (httpResponse.status == 401) {
    window.location.replace("user/signIn");
  }
}

async function refreshTokenToToken() {
  const get = async () => {
    if (window.location.pathname != "/user/signIn") {
      let sessionToken = Cookies.get("sessionToken");
      let expiresIn = 0;
      if (sessionToken) {
        expiresIn =
          (jwtDecode(sessionToken) as any).exp - Math.floor(Date.now() / 1000);
      }
      if (expiresIn < 300) {
        let result = await fetch(
          `${import.meta.env.VITE_API_URL}/user/sessionToken`,
          {
            credentials: "include" as RequestCredentials,
            method: "POST",
            headers: [["Content-Type", "application/json"]],
          }
        );
        if (result.status == 401) {
          window.location.replace("/user/signIn");
        }
      }
    }
  };
  await get();
  setInterval(() => get(), 60000);
}
export default {
  get,
  list,
  changePassword,
  signOut,
  updateUser,
  resetPassword,
  signUp,
  deleteUser,
  listRoles,
  checkIfSignedIn,
  refreshTokenToToken,
};
