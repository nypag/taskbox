import responseHandler from "../utils/fetchResponseHandler";

async function getUserStatus() {
  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/integrations/daellenbach/status`,
    {
      credentials: "include" as RequestCredentials,
      method: "GET",
      headers: [["Content-Type", "application/json"]],
    }
  );

  return await responseHandler(result);
}

async function inscribe(
  portions: number,
  autoInscribe: boolean,
  status: boolean
) {
  if (!status) {
    portions = 0;
  }
  let result = await fetch(
    `${import.meta.env.VITE_API_URL}/integrations/daellenbach/inscribe`,
    {
      credentials: "include" as RequestCredentials,
      method: "POST",
      headers: [["Content-Type", "application/json"]],
      body: JSON.stringify({ portions, autoInscribe }),
    }
  );

  return await responseHandler(result, true);
}

export default {
  getUserStatus,
  inscribe,
};
