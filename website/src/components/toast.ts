import { toast } from "@zerodevx/svelte-toast";

function error(message: string) {
  toast.push(message, {
    theme: {
      "--toastBackground": "#F56565",
      "--toastBarBackground": "#C53030",
      " --toastContainerRight": "auto",
      " --toastContainerBottom": "8rem",
      "--toastContainerTop": "auto",
      "--toastContainerLeft": "calc(50vw - 8rem)",
    },
  });
}

function success(message: string) {
  toast.push(message, {
    theme: {
      "--toastBackground": "#48BB78",
      "--toastBarBackground": "#2F855A",
      " --toastContainerRight": "auto",
      " --toastContainerBottom": "8rem",
    },
  });
}

export default {
  error,
  success,
};
