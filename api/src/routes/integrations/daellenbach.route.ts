import express from "express";
import checkAuthorization from "../../middlewares/checkAuthorization.middleware";
const router = express.Router();
import daellenbach from "../../controller/integrations/daellenbach.controller";

router.post("/inscribe", checkAuthorization("daellenbach_subscribe"), daellenbach.inscribe);
router.get("/status", checkAuthorization("daellenbach_subscribe"), daellenbach.getUserStatus);

export default router;
