import express from "express";
import checkAuthorization from "../middlewares/checkAuthorization.middleware";
import role from "../controller/role.controller";
const router = express.Router();

router.get("/", checkAuthorization("user_write"), role.list);

router.get("/:user_role_id", checkAuthorization("user_write"), role.get);

router.patch("/:user_role_id", checkAuthorization("user_write"), role.update);

router.delete("/:user_role_id", checkAuthorization("user_write"), role.remove);

export default router;
