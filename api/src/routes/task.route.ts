import express from "express";
import checkAuthorization from "../middlewares/checkAuthorization.middleware";
import task from "../controller/task.controller";
const router = express.Router();

router.post("/", checkAuthorization("task_write"), task.create);

router.get("/", checkAuthorization("task_write"), task.list);
router.get("/:task_id", checkAuthorization("task_write"), task.get);

router.patch("/:task_id", checkAuthorization("task_write"), task.update);

router.delete("/:task_id", checkAuthorization("task_write"), task.remove);

export default router;
