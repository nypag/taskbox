import express from "express";
import checkAuthorization from "../middlewares/checkAuthorization.middleware";
import userTask from "../controller/userTask.controller";
const router = express.Router();

router.post("/", checkAuthorization("userTask_write"), userTask.create);

router.get(["/", "/:user_task_id"], userTask.list);

router.get(
  "/undoneTasks/:user_id",
  checkAuthorization("same_userid"),
  userTask.getUndoneTasks
);

router.patch(
  "/:user_task_id",
  checkAuthorization("userTask_write"),
  userTask.update
);

router.delete(
  "/:user_task_id",
  checkAuthorization("userTask_write"),
  userTask.remove
);

router.patch(
  "/:task_id/changeStatus",
  checkAuthorization("userTask_changeStatus"),
  userTask.changeStatus
);

export default router;
