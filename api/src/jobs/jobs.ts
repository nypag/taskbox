import cleanUserSessions from "./db/cleanUserTasks.job";
import cleanUserTasks from "./db/cleanUserSessions.job";
import daellenbachMail from "./daellenbachMail.job";

export default function () {
  if (process.env.NODE_ENV == "prod") {
    cleanUserSessions();
    cleanUserTasks();
    daellenbachMail();
  }
}
