import schedule from "node-schedule";
import daellenbachService from "../service/integrations/daellenbach.service";

const sendDaellenbachMail = () =>
  schedule.scheduleJob("0 10 * * 3-5", async function () {
    await daellenbachService.sendMail();
    console.log("[SCHEDULE] Sent Dällenbach Mail.");
  });

export default sendDaellenbachMail;
