import schedule from "node-schedule";
import { prisma } from "../../utils/prisma.util";

const cleanUserTasks = () =>
  schedule.scheduleJob("0 1 * * *", async function () {
    const oneMonthAgo = new Date(new Date().setDate(new Date().getDate() - 30));
    const result = await prisma.user_tasks.deleteMany({
      where: {
        date: { lte: oneMonthAgo },
      },
    });

    console.log(`[SCHEDULE] Deleted ${result.count} user tasks.`);
  });

export default cleanUserTasks;
