import schedule from "node-schedule";
import { prisma } from "../../utils/prisma.util";

const cleanUserSessions = () =>
  schedule.scheduleJob("0 1 * * *", async function () {
    const result = await prisma.refresh_tokens.deleteMany({
      where: {
        expires_at: { lt: new Date() },
      },
    });

    console.log(`[SCHEDULE] Deleted ${result.count} refresh tokens..`);
  });

export default cleanUserSessions;
