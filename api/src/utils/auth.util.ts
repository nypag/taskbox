import crypto from "crypto";
import { extractJWT } from "./jwt.util";
import { prisma } from "./prisma.util";

function hashPW(
  password: crypto.BinaryLike,
  salt = crypto.randomBytes(16).toString("hex")
) {
  // Hashing user's salt and password with 1000 iterations,

  const hash = crypto
    .pbkdf2Sync(password, salt, 1000, 64, "sha512")
    .toString("hex");

  return { hash, salt };
}

async function getUserBySession(sessionToken: string) {
  return (await extractJWT(sessionToken)).payload.userId as number;
}

async function checkPermission(sessionToken: string, scopes: Array<string>) {
  let permissionOk = false;
  try {
    const user_id = await getUserBySession(sessionToken);
    for (const scope of scopes) {
      const scopeFilter = JSON.parse(`{"${scope}": true}`);

      const result = await prisma.users.findFirst({
        select: {
          role: {
            select: {
              permissions: true,
            },
          },
        },
        where: {
          AND: [
            { user_id: user_id },
            {
              role: {
                permissions: scopeFilter,
              },
            },
          ],
        },
      });

      if (result != null) {
        permissionOk = true;
        return true;
      }
    }
    if (!permissionOk) return false;
  } catch (_) {
    console.log(_);
    return false;
  }
}

async function deleteSessions(user_id: number) {
  try {
    await prisma.refresh_tokens.deleteMany({
      where: {
        id_user: user_id,
      },
    });
    return true;
  } catch {
    return false;
  }
}

export default module.exports = {
  hashPW: hashPW,
  getUserBySession,
  checkPermission,
  deleteSessions,
};
