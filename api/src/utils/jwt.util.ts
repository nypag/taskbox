import * as jwt from "jose";

export const createJWT = async (
  userId: number,
  type: "session" | "refresh" = "session"
) => {
  const expiration =
    Math.floor(Date.now() / 1000) +
    (type == "session" ? 60 * 60 : 60 * 60 * 24 * 28);

  const token = new jwt.SignJWT({ userId, type })
    .setExpirationTime(expiration)
    .setProtectedHeader({ alg: "HS256" })
    .sign(Buffer.from(process.env.JWT_SECRET));
  return token;
};

export const verifyJWT = async (token: string | undefined) => {
  if (!token) return false;

  try {
    await jwt.jwtVerify(token, Buffer.from(process.env.JWT_SECRET));
    return true;
  } catch {
    return false;
  }
};

export const extractJWT = async (token: string | undefined) => {
  try {
    return await jwt.jwtVerify(token, Buffer.from(process.env.JWT_SECRET));
  } catch {
    throw "Invalid or no session token";
  }
};
