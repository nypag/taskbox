import express from "express";
import dotenv from "dotenv";
import rateLimit from "./middlewares/rateLimit.middleware";
import bodyParser from "body-parser";
import cors from "cors";
import jobs from "./jobs/jobs";

import role_route from "./routes/role.route";
import task_route from "./routes/task.route";
import daellenbach_route from "./routes/integrations/daellenbach.route";
import user_route from "./routes/user.route";
import userTask_route from "./routes/userTask.route";
import isLoggedIn from "./middlewares/isLoggedIn.middleware";
import cookieParser from "cookie-parser";

//Initialize dot env
dotenv.config();

// Schedule the background jobs
jobs();

//Initialize express
const app = express();

// Set cors
app.use(
  cors({
    allowedHeaders: ["authorization", "content-type"],
    origin: process.env.FRONTEND_HOST,
    credentials: true,
  })
);

// Global Middlewares
app.use(bodyParser.json());

app.use(cookieParser());

app.use(rateLimit.global);

// Routes
app.use("/user", user_route);

// Middleware to make sure that client is logged in
app.use(isLoggedIn);

app.use("/role", role_route);
app.use("/integrations/daellenbach", daellenbach_route);

app.use("/task", task_route);

app.use("/userTask", userTask_route);

// Start the http server
app.listen(8080, () => {
  console.info(
    `The api is listening at http://localhost:8080 in "${process.env.NODE_ENV}" mode 🚀`
  );
});
