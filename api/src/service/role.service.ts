import { Prisma } from "@prisma/client";
import { prisma } from "../utils/prisma.util";

async function list() {
  return await prisma.user_roles.findMany({
    select: {
      user_role_id: true,
      name: true,
      permissions: true,
    },
    take: 50,
  });
}

async function get(user_role_id: number) {
  return await prisma.user_roles.findUnique({
    where: {
      user_role_id: user_role_id,
    },
    select: {
      user_role_id: true,
      name: true,
      permissions: true,
    },
  });
}

async function remove(user_role_id: number) {
  return await prisma.user_roles.delete({
    where: {
      user_role_id: user_role_id,
    },
  });
}

async function update(
  user_role_id: number,
  data: Prisma.user_rolesUpdateInput
) {
  return await prisma.user_roles.update({
    data: {
      name: data.name,
      permissions: {
        update: data.permissions,
      },
    },
    where: {
      user_role_id: user_role_id,
    },
  });
}

async function create(data: Prisma.user_rolesCreateInput) {
  return prisma.user_roles.create({
    data: {
      name: data.name,
      permissions: {
        create: data.permissions,
      },
    },
  });
}

export default {
  list,
  update,
  remove,
  create,
  get,
};
