import { prisma } from "../../utils/prisma.util";
import sendGrid from "@sendgrid/mail";
import { Prisma } from "@prisma/client";
sendGrid.setApiKey(process.env.SENDGRID_API_KEY);

const msg = (count: number) => {
  return {
    to: "ali.doroudchi@noseryoung.ch",
    from: "ali.doroudchi@noseryoung.ch",
    subject: "Mittagessen Noser Young",
    text: `Guten Morgen

Heute werden ${count} Lernende den Dällebach besuchen.
    
Mit freundlichen Grüssen
Ali Doroudchi`,
  };
};

const inscribe = async (
  userId: number,
  portions: number,
  autoInscribe: boolean
) => {
  await prisma.integration_daellenbach.upsert({
    update: {
      portions: portions,
      autoInscribe: autoInscribe,
    },
    create: {
      id_user: userId,
      portions: portions,
      autoInscribe: autoInscribe,
    },
    where: {
      id_user: userId,
    },
  });
};

const getUserStatus = async (userId: number) => {
  const result = await prisma.integration_daellenbach.findUnique({
    where: {
      id_user: userId,
    },
  });

  if (result && result.portions != 0) {
    return {
      portions: result.portions,
      status: true,
      autoInscribe: result.autoInscribe,
      portionCount: await getPortionCount(),
    };
  } else {
    return {
      portions: 0,
      status: false,
      autoInscribe: false,
      portionCount: await getPortionCount(),
    };
  }
};

const sendMail = async () => {
  const portionCount = await getPortionCount();
  if (portionCount > 0) {
    await sendGrid.send(msg(portionCount));
  }

  await cleanUp();
};

const cleanUp = async () => {
  const filters: Prisma.integration_daellenbachWhereInput = {};
  // If friday delete all inscriptions
  if (new Date().getDay() != 5) {
    filters.autoInscribe = {
      not: true,
    };
  }
  await prisma.integration_daellenbach.deleteMany({
    where: filters,
  });
};

const getPortionCount = async () => {
  let portionCount = 0;
  (
    await prisma.integration_daellenbach.findMany({
      select: { portions: true },
    })
  ).forEach((portion) => {
    portionCount += portion.portions;
  });
  return portionCount;
};

export default { inscribe, sendMail, getUserStatus };
