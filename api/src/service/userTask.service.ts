import { prisma } from "../utils/prisma.util";
import auth from "../utils/auth.util";
import { Prisma } from "@prisma/client";
import dateTimeUtil from "../utils/dateTime.util";

async function create(data: Prisma.user_tasksCreateManyInput) {
  return await prisma.user_tasks.createMany({
    data: data,
  });
}

const get = async (user_task_id: number) => {
  return await prisma.user_tasks.findUnique({
    where: {
      user_task_id: user_task_id,
    },
  });
};

async function update(
  user_task_id: number,
  data: Prisma.user_tasksUpdateInput
) {
  return await prisma.user_tasks.update({
    data: data,
    where: {
      user_task_id: user_task_id,
    },
  });
}

async function remove(user_task_id: number) {
  await prisma.user_tasks.delete({
    where: { user_task_id: user_task_id },
  });
}

async function list(filters: any, sortOrder: Prisma.SortOrder = "asc") {
  const result = await prisma.user_tasks.findMany({
    select: {
      user_task_id: true,
      isDone: true,
      date: true,
      user_assigned: {
        select: {
          user_id: true,
          first_name: true,
          last_name: true,
        },
      },
      user_assigned_dep: {
        select: {
          user_id: true,
          first_name: true,
          last_name: true,
        },
      },
      user_status_updated_by: {
        select: {
          user_id: true,
          first_name: true,
          last_name: true,
        },
      },
      task: {
        select: {
          task_id: true,
          name: true,
          description: true,
          img_name: true,
          hasStatus: true,
          hotkey: true,
          deadline_time: true,
        },
      },
    },
    where: filters,
    orderBy: [
      {
        date: sortOrder,
      },
      {
        task: {
          deadline_time: "asc",
        },
      },
      {
        task: {
          hasStatus: "desc",
        },
      },
      {
        task: {
          name: "asc",
        },
      },
    ],
  });
  // Concat the the deadline time and deadline date
  for (const task of result) {
    const hour = task.task.deadline_time.getHours();
    const minute = task.task.deadline_time.getHours();
    task.date.setHours(hour);
    task.date.setMinutes(minute);
  }
  return result;
}

async function changeStatus(
  task_id: number,
  incomingStatus: boolean,
  session_token: string
) {
  const user_id = await auth.getUserBySession(session_token);
  const today = dateTimeUtil.dateTimeToDate(new Date(Date.now()));

  const user_task_id = (
    await prisma.user_tasks.findFirst({
      where: {
        AND: [{ id_task: task_id }, { date: today }],
      },
    })
  ).user_task_id;

  let currentStatus;

  if (incomingStatus == undefined) {
    currentStatus = (
      await prisma.user_tasks.findUnique({
        where: {
          user_task_id: user_task_id,
        },
      })
    ).isDone;
  }

  return await prisma.user_tasks.update({
    data: {
      status_updated_by: user_id,
      isDone: !currentStatus,
    },
    where: {
      user_task_id: user_task_id,
    },
  });
}

async function getUndoneTasks(user_id: number) {
  const result = await prisma.user_tasks.findMany({
    where: {
      id_user: user_id,
      task: {
        hasStatus: true,
      },
      date: { lt: new Date() },
      isDone: false,
    },
    take: 100,
  });

  return {
    sum: result.length,
    userTasks: result,
  };
}

export default module.exports = {
  get,
  create,
  list,
  update,
  remove,
  changeStatus,
  getUndoneTasks,
};
