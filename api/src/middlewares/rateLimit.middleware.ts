import rateLimit from "express-rate-limit";

function custom(maxRequests: number, time: number) {
  return rateLimit({
    windowMs: time * 60 * 1000,
    max: maxRequests,
    message: "Zu viele Requests. Bitte versuche es später erneut.",
    standardHeaders: true,
    legacyHeaders: false,
  });
}

const global = rateLimit({
  windowMs: 60 * 1000 * 5, // 5 minutes
  max: 500,
  standardHeaders: true,
  message: "Zu viele Anfragen von dieser IP. Versuche es in 5 Minuten erneut.",
  legacyHeaders: false,
});

export default {
  custom,
  global,
};
