import { NextFunction, Request, Response } from "express";
import { verifyJWT } from "../utils/jwt.util";

const isLoggedIn = async (req: Request, res: Response, next: NextFunction) => {
  const sessionToken = req.cookies?.sessionToken;

  if (await verifyJWT(sessionToken)) {
    next();
  } else {
    return res.sendStatus(401);
  }
};

export default isLoggedIn;
