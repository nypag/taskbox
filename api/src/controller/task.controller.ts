import { Request, Response } from "express";
import task from "../service/task.service";

async function create(req: Request, res: Response) {
  const data = req.body;

  try {
    const { task_id } = await task.create(data);

    res.status(201).send(await task.get(task_id));
  } catch (_) {
    console.log(_);
    res.status(400).send("Fehler beim Erstellen eines Ämtlis.");
  }
}

async function update(req: Request, res: Response) {
  const data = req.body;
  const task_id = parseInt(req.params.task_id);
  try {
    await task.update(task_id, data);
    res.status(200).send(await task.get(task_id));
  } catch (_) {
    res.status(400).send("Fehler beim Aktualisieren eines Ämtlis.");
  }
}

async function remove(req: Request, res: Response) {
  const task_id = parseInt(req.params.task_id);
  try {
    await task.remove(task_id);
    res.sendStatus(204);
  } catch (_) {
    res.status(400).send("Fehler beim Löschen eines Ämtlis.");
  }
}

async function list(req: Request, res: Response) {
  try {
    const result = await task.list();
    res.status(200).send(result);
  } catch (_) {
    res.status(500);
  }
}

async function get(req: Request, res: Response) {
  const task_id = parseInt(req.params.task_id);
  try {
    const result = await task.get(task_id);
    res.status(200).send(result);
  } catch (_) {
    res.status(500);
  }
}

export default module.exports = {
  create,
  list,
  update,
  remove,
  get,
};
