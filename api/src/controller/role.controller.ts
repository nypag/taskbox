import { Request, Response } from "express";
import role from "../service/role.service";

async function list(req: Request, res: Response) {
  try {
    const result = await role.list();
    res.send(result);
  } catch (_) {
    res.sendStatus(500);
  }
}

async function get(req: Request, res: Response) {
  const user_role_id = parseInt(req.params.user_role_id);
  try {
    const result = await role.get(user_role_id);
    res.send(result);
  } catch (_) {
    res.sendStatus(500);
  }
}

async function remove(req: Request, res: Response) {
  const user_role_id = parseInt(req.params.user_role_id);
  try {
    await role.remove(user_role_id);
    res.sendStatus(201);
  } catch (_) {
    res.sendStatus(500);
  }
}

async function update(req: Request, res: Response) {
  const user_role_id = parseInt(req.params.user_role_id);
  const data = req.body;
  try {
    await role.update(user_role_id, data);

    res.status(200).send(await role.get(user_role_id));
  } catch (_) {
    console.log(_);
    res.sendStatus(500);
  }
}

async function create(req: Request, res: Response) {
  const data = req.body;
  try {
    const { user_role_id } = await role.create(data);
    res.status(201).send(await role.get(user_role_id));
  } catch (_) {
    res.sendStatus(500);
  }
}

export default {
  list,
  update,
  remove,
  create,
  get,
};
