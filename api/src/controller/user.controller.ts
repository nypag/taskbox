import { Request, Response } from "express";
import user from "../service/user.service";
import cookie from "cookie";

async function create(req: Request, res: Response) {
  const data = req.body;
  try {
    const result = await user.create(data);

    res.status(201).send(await user.get(result.user_id));
  } catch (_) {
    res.status(400).send("Failed to create user.");
  }
}

async function signIn(req: Request, res: Response) {
  const { email } = req.body;
  const { password } = req.body;

  try {
    const tokens = await user.signIn(email, password);

    res.setHeader("Set-Cookie", [
      cookie.serialize("sessionToken", tokens.sessionToken, {
        path: "/",
        secure: true,
        maxAge: 60 * 60, // 1 hour
      }),
      cookie.serialize("refreshToken", tokens.refreshToken, {
        httpOnly: true,
        path: process.env.PROXY_PATH + "/user/sessionToken",
        secure: true,
        maxAge: 60 * 60 * 24 * 28, // 4 weeks
      }),
    ]);
    res.send(tokens);
  } catch {
    res.status(401).send("Falsche Email oder Passwort.");
  }
}

async function update(req: Request, res: Response) {
  const user_id = parseInt(req.params.user_id);
  const data = req.body;
  try {
    await user.update(user_id, data);
    res.status(200).send(await user.get(user_id));
  } catch (_) {
    console.log(_);
    res.status(400).send("Fehler beim aktualisieren eines Benutzers.");
  }
}
async function remove(req: Request, res: Response) {
  const user_id = parseInt(req.params.user_id);
  try {
    await user.remove(user_id);
    res.sendStatus(204);
  } catch (_) {
    res.status(400).send("Fehler beim löschen eines Benutzers.");
  }
}

async function changePassword(req: Request, res: Response) {
  const { sessionToken } = req.cookies;
  const { oldPassword } = req.body;
  const { newPassword } = req.body;
  try {
    await user.changePassword(sessionToken, oldPassword, newPassword);
    res.send("Das Passwort wurde erfolgreich geändert.");
  } catch {
    res.status(401).send("Falsches Passwort.");
  }
}

async function resetPassword(req: Request, res: Response) {
  const { user_id } = req.body;
  const { newPassword } = req.body;
  try {
    await user.resetPassword(user_id, newPassword);
    res.send("Password erfolgreich geändert.");
  } catch (e) {
    res.sendStatus(500);
  }
}

async function list(req: Request, res: Response) {
  const showHiddenUsers = JSON.parse(
    (req.query.showHiddenUsers as string).toLowerCase()
  );
  try {
    res.status(200).send(await user.list(showHiddenUsers));
  } catch (_) {
    console.log(_);
    res.sendStatus(500);
  }
}
async function get(req: Request, res: Response) {
  const session_token = req.cookies.sessionToken;

  const user_id = parseInt(req.params.user_id);

  try {
    if (isNaN(user_id)) {
      res.status(200).send(await user.getBySessionToken(session_token));
    } else {
      res.status(200).send(await user.get(user_id));
    }
  } catch (e) {
    if (e == "Invalid or no session token") {
      return res.sendStatus(401);
    }
    console.log(e);
  }
}

async function refreshTokenToToken(req: Request, res: Response) {
  const { refreshToken } = req.cookies;
  try {
    const sessionToken = await user.refreshTokenToToken(refreshToken);
    res.setHeader(
      "Set-Cookie",
      cookie.serialize("sessionToken", sessionToken, {
        path: "/",
        secure: true,
        maxAge: 60 * 60, // 1 hour
      })
    );
    res.send({ sessionToken });
  } catch {
    res.sendStatus(401);
  }
}

async function signOut(req: Request, res: Response) {
  try {
    res.setHeader("Set-Cookie", [
      cookie.serialize("refreshToken", "", {
        path: process.env.PROXY_PATH + "/user/sessionToken",
        maxAge: 1,
      }),
      cookie.serialize("sessionToken", "", {
        path: "/",
        maxAge: 1,
      }),
    ]);
    res.sendStatus(200);
  } catch {
    res.sendStatus(401);
  }
}

export default module.exports = {
  signIn,
  create,
  get,
  list,
  changePassword,
  resetPassword,
  update,
  remove,
  signOut,
  refreshTokenToToken,
};
