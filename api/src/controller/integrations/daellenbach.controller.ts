import { Request, Response } from "express";
import authUtil from "../../utils/auth.util";
import daellenbach from "../../service/integrations/daellenbach.service";

async function inscribe(req: Request, res: Response) {
  const { portions } = req.body;
  const { autoInscribe } = req.body;
  const userId = await authUtil.getUserBySession(req.cookies.sessionToken);

  try {
    if (portions < 0 || portions > 10)
      return res.status(400).send("Ungültige Anzahl der Portionen.");
    await daellenbach.inscribe(userId, portions, autoInscribe);
    res.sendStatus(201);
  } catch (_) {
    res.sendStatus(500);
  }
}

async function getUserStatus(req: Request, res: Response) {
  const userId = await authUtil.getUserBySession(req.cookies.sessionToken);
  try {
    res.send(await daellenbach.getUserStatus(userId));
  } catch (_) {
    res.sendStatus(500);
  }
}

export default {
  inscribe,
  getUserStatus,
};
