# Taskbox api

The taskbox is written in Type Script and uses the libraries express and socketio.

## Run the api

1. Install node js
2. install all packages with `npm install`
3. Start the server with `npm run start`

## Work ont the project

Note: Please run `npx prettier --write . ` to format the code before comitting
