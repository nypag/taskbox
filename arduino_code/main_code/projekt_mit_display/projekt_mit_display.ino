#include <doxygen.h>
#include <NexButton.h>
#include <NexCheckbox.h>
#include <NexConfig.h>
#include <NexCrop.h>
#include <NexDualStateButton.h>
#include <NexGauge.h>
#include <NexGpio.h>
#include <NexHardware.h>
#include <NexHotspot.h>
#include <NexNumber.h>
#include <NexObject.h>
#include <NexPage.h>
#include <NexPicture.h>
#include <NexProgressBar.h>
#include <NexRadio.h>
#include <NexRtc.h>
#include <NexScrolltext.h>
#include <NexSlider.h>
#include <NexText.h>
#include <NexTimer.h>
#include <Nextion.h>
#include <NexTouch.h>
#include <NexUpload.h>
#include <NexVariable.h>
#include <NexWaveform.h>
#include <Nextion.h>
#include <Wire.h>
#include <HTTPClient.h>
#include <WiFiUdp.h>
#include <SoftwareSerial.h>

SoftwareSerial nextion(21, 22); // RX, TX
String daten_display = "";

HTTPClient http;

//wifi
const char *ssid = "GuestWLANPortal";
const char *password = "";

const byte led_gpio_green = 32;

void setup() {
  Serial.begin(9600);
  nextion.begin(9600);

  pinMode(led_gpio_green, OUTPUT);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  { //Check for the connection
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  Serial.println("Connected to the WiFi network");

}

void loop() {

  if (nextion.available())
  {
    daten_display += char (nextion.read());
  }

  if (daten_display == "Button 1") {
    Serial.println("Facility is done");
    changeData("1");
    daten_display = "";

  }

  if (daten_display == "Button 2") {
    Serial.println("Post Morgen is done");
    changeData("2");
    daten_display = "";

  }

  if (daten_display == "Button 3") {
    Serial.println("Post Abend is done");
    changeData("3");
    daten_display = "";

  }

  if (daten_display == "Button 4") {
    Serial.println("Backup is done");
    changeData("4");
    daten_display = "";

  }

  if (daten_display == "Button 5") {
    Serial.println("Aktivierung is done");
    changeData("5");
    daten_display = "";

  }

  if (daten_display == "Button 6") {
    Serial.println("USB-Stick is done");
    changeData("6");
    daten_display = "";

  }

  if (WiFi.status() != WL_CONNECTED)
  {
    WiFi.begin(ssid, password);
  }

  if (WiFi.status() == WL_CONNECTED)
  {
    digitalWrite(led_gpio_green, HIGH);
  }
  else
  {
    digitalWrite(led_gpio_green, LOW);
  }
}

void changeData(String tasknumber)
{
  if (WiFi.status() == WL_CONNECTED)
  { //Check WiFi connection status

    http.begin(String("https://airq.noseryoung.ch/api/userTask/") + tasknumber + "/changeStatus"); //Specify destination for HTTP request
    http.addHeader("Content-Type", "application/json");
    http.addHeader("Authorization", "Bearer 5a383f186c4d8ac26577deffaca11d6059fe20f45c5eaf9d9dc6073394ebe94be175b5ca947f98230a5b5d4c28599f06b3561db07aaceaf879b3cad2da7bfd3c0f4eb96a5645d08acef4b35667cb207fb9bbdc30aa6a362f3fdb67a202dd9a8b47c7932b79f620bb8c207636ce9a681e6bfc62e57084a32bef4a");
    int httpResponseCode = http.PATCH(String("{\"isDone\":") + "true" + "}");

    if (httpResponseCode > 0)
    {
      String response = http.getString(); //Get the response to the request
      Serial.println(httpResponseCode); //Print return code
      Serial.println(response);         //Print request answer
    }
    else
    {
      Serial.print("Error on sending POST: ");
      Serial.println(httpResponseCode);
    }

    http.end(); //Free resources
  }
  else
  {
    Serial.println("Error in WiFi connection");
  }
}
