#include <ESP32HTTPUpdateServer.h>
#include <EspMQTTClient.h>
#include <Keypad.h>
#include <RTCDS1307.h>
#include <Wire.h>


#define ROW_NUM     4 // four rows
#define COLUMN_NUM  3 // three columns

EspMQTTClient client(
 "GuestWLANPortal",
 "",
 // "homeland",
  "142.93.174.193",  // MQTT Broker server ip
  //"",   // Can be omitted if not needed
  //"",   // Can be omitted if not needed
  "Taskbox_ESP32",     // Client name that uniquely identify your device
  1883              // The MQTT port, default to 1883. this line can be omitted
);

//led_pins
const byte led_gpio_red1 = 19;
const byte led_gpio_green1 = 21;
const byte led_gpio_red2 = 32;
const byte led_gpio_green2 = 33;
const byte led_gpio_red3 = 25;
const byte led_gpio_green3 = 26;
const byte led_gpio_red4 = 27;
const byte led_gpio_green4 = 14;
const byte led_gpio_red5 = 12;
const byte led_gpio_green5 = 13;

char keys[ROW_NUM][COLUMN_NUM] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

byte pin_rows[ROW_NUM] = {18, 5, 17, 16}; // GIOP18, GIOP5, GIOP17, GIOP16 connect to the row pins
byte pin_column[COLUMN_NUM] = {4, 0, 2};  // GIOP4, GIOP0, GIOP2 connect to the column pins

 
Keypad keypad = Keypad( makeKeymap(keys), pin_rows, pin_column, ROW_NUM, COLUMN_NUM );

void setup() {
Serial.begin(9600);


client.enableHTTPWebUpdater(); // Enable the web updater. User and password default to values of MQTTUsername and MQTTPassword. These can be overrited with enableHTTPWebUpdater("user", "password").
client.enableLastWillMessage("worblaufen/taskbox", "RIP");  // You can activate the retain flag by setting the third parameter to true

pinMode(led_gpio_red1, OUTPUT);
pinMode(led_gpio_green1, OUTPUT);
pinMode(led_gpio_red2, OUTPUT);
pinMode(led_gpio_green2, OUTPUT);
pinMode(led_gpio_red3, OUTPUT);
pinMode(led_gpio_green3, OUTPUT);
pinMode(led_gpio_red4, OUTPUT);
pinMode(led_gpio_green4, OUTPUT);
pinMode(led_gpio_red5, OUTPUT);
pinMode(led_gpio_green5, OUTPUT);

digitalWrite(led_gpio_red1,HIGH);
digitalWrite(led_gpio_red2,HIGH);
digitalWrite(led_gpio_red3,HIGH);
digitalWrite(led_gpio_red4,HIGH);
digitalWrite(led_gpio_red5,HIGH);

}

void onConnectionEstablished()
{
  client.subscribe("worblaufen/taskbox/reset", [](const String & payload){
  Serial.println(payload); 
  if (payload == "9") 
  {
digitalWrite(led_gpio_red1,HIGH);
digitalWrite(led_gpio_red2,HIGH);
digitalWrite(led_gpio_red3,HIGH);
digitalWrite(led_gpio_red4,HIGH);
digitalWrite(led_gpio_red5,HIGH);
digitalWrite(led_gpio_green1,LOW);
digitalWrite(led_gpio_green2,LOW);
digitalWrite(led_gpio_green3,LOW);
digitalWrite(led_gpio_green4,LOW);
digitalWrite(led_gpio_green5,LOW);
  }
 }); 
}

void loop() 
{
  
char key = keypad.getKey();

if (key) {
  Serial.print(key);
  if (key == '1')
  {
    if (digitalRead(led_gpio_red1) == HIGH )
    {
    digitalWrite(led_gpio_red1,LOW);
    client.publish("worblaufen/taskbox/post", "11");
    digitalWrite(led_gpio_green1,HIGH);
    }
    else{
       digitalWrite(led_gpio_red1,HIGH);
    client.publish("worblaufen/taskbox/post", "10");
    digitalWrite(led_gpio_green1,LOW);
      }
    }

if (key == '2')
  {
    if (digitalRead(led_gpio_red2) == HIGH )
    {
    digitalWrite(led_gpio_red2,LOW);
    client.publish("worblaufen/taskbox/facility", "21");
    digitalWrite(led_gpio_green2,HIGH);
    }
    else{
       digitalWrite(led_gpio_red2,HIGH);
    client.publish("worblaufen/taskbox/facility", "20");
    digitalWrite(led_gpio_green2,LOW);
      }
    }

if (key == '7')
  {
    if (digitalRead(led_gpio_red3) == HIGH )
    {
    digitalWrite(led_gpio_red3,LOW);
    client.publish("worblaufen/taskbox/aktivierung", "71");
    digitalWrite(led_gpio_green3,HIGH);
    }
    else{
       digitalWrite(led_gpio_red3,HIGH);
    client.publish("worblaufen/taskbox/aktivierung", "70");
    digitalWrite(led_gpio_green3,LOW);
      }
    }

if (key == '4')
  {
    if (digitalRead(led_gpio_red4) == HIGH )
    {
    digitalWrite(led_gpio_red4,LOW);
    client.publish("worblaufen/taskbox/usb-stick", "41");
    digitalWrite(led_gpio_green4,HIGH);
    }
    else{
       digitalWrite(led_gpio_red4,HIGH);
    client.publish("worblaufen/taskbox/usb-stick", "40");
    digitalWrite(led_gpio_green4,LOW);
      }
    }

if (key == '5')
  {
    if (digitalRead(led_gpio_red5) == HIGH )
    {
    digitalWrite(led_gpio_red5,LOW);
    client.publish("worblaufen/taskbox/backup", "51");
    digitalWrite(led_gpio_green5,HIGH);
    }
    else{
       digitalWrite(led_gpio_red5,HIGH);
    client.publish("worblaufen/taskbox/backup", "50");
    digitalWrite(led_gpio_green5,LOW);
      }
    }
  }
 client.loop();
}
