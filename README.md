# Ämtli Plan

## Projekt Struktur

    .
    ├── api                   # Das Api (Node JS)
    ├── arduino_code          # Der Arduino Code für den Touchscreen (C++)
    ├── assets                # Dateien wie das 3D Modell, Logo und 3D Modelle
    ├── db                    # Datenbank relevante Dateien wie Dumps, Backup Scipt und ERD
    ├── website               # Das Frontend (SvelteKit TS)
    ├── .env                  # Env File für Docker
    ├── docker-compose.yml    # Docker Compose File um Docker Images zu erstellen
    └── README.md

### Api

Das Api wurde mit Node JS (TS) und dem Package Express erstellt.

### Datenbank

Die Datenbank ist eine MariaDB.

### Website

Die Website wurde mit SvelteKit und TS erstellt.
