/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `user.write` tinyint(1) NOT NULL DEFAULT 0,
  `task.write` tinyint(1) NOT NULL DEFAULT 0,
  `userTask.write` tinyint(1) NOT NULL DEFAULT 0,
  `userTask.viewHistory` tinyint(1) NOT NULL DEFAULT 0,
  `userTask.changeStatus` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`permission_id`),
  UNIQUE KEY `permissions_pid_uindex` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,1,1,1,1,1),(2,0,1,1,0,1),(3,0,0,0,0,1),(4,0,0,0,0,1),(5,0,0,1,0,1),(6,1,1,1,0,1),(7,0,0,0,1,0);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `img_name` varchar(60) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `deadline_time` time DEFAULT NULL,
  `hasStatus` tinyint(1) DEFAULT 1,
  `hotkey` char(1) DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  UNIQUE KEY `tasks_tid_uindex` (`task_id`),
  UNIQUE KEY `tasks_hotkey_uindex` (`hotkey`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (1,'Facility','facility.svg',NULL,'10:30:00',1,'3'),(2,'Post (Morgens)','post_morgen.svg',NULL,'11:30:00',1,'4'),(3,'Post (Abends)','post_abend.svg',NULL,'16:45:00',1,'5'),(4,'Backup','backup.svg',NULL,'10:00:00',1,'2'),(5,'Aktivierung','aktivierung.svg',NULL,'08:00:00',1,'1'),(6,'Hilfe Pesche','hilfe_pesche.svg',NULL,'17:00:00',0,NULL),(7,'Desinfektion','desinfektion.svg',NULL,'17:00:00',0,NULL),(8,'Telefon','telefon.svg',NULL,'17:00:00',0,NULL),(9,'Support','support.svg',NULL,'17:00:00',0,NULL),(10,'Fotoprotokoll','fotoprotokoll.svg',NULL,'17:00:00',0,NULL),(11,'USB-Stick','usb.svg',NULL,'17:00:00',0,NULL),(12,'Putzchef','putzchef.svg',NULL,'17:00:00',0,NULL);
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `id_permission` int(11) DEFAULT NULL,
  `isAssignable` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`user_role_id`),
  UNIQUE KEY `Roles_rid_uindex` (`user_role_id`),
  KEY `groups_permissions_pid_fk` (`id_permission`),
  CONSTRAINT `groups_permissions_pid_fk` FOREIGN KEY (`id_permission`) REFERENCES `permissions` (`permission_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'Admin',1,1),(2,'Ämtli Master',2,1),(3,'Benutzer',3,1),(4,'Device',4,0),(5,'Ämtli Editor',5,1),(6,'Berufsbildner',6,0),(7,'Externer Benutzer',7,0);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_sessions`
--

DROP TABLE IF EXISTS `user_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_sessions` (
  `session_token` char(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `expire_at` datetime NOT NULL DEFAULT (current_timestamp() + interval 30 day),
  PRIMARY KEY (`session_token`),
  UNIQUE KEY `user_sessions_session_id_uindex` (`session_token`),
  KEY `user_sessions_users_uid_fk` (`id_user`),
  CONSTRAINT `user_sessions_users_uid_fk` FOREIGN KEY (`id_user`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `user_tasks`
--

DROP TABLE IF EXISTS `user_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_tasks` (
  `user_task_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_user_dep` int(11) DEFAULT NULL,
  `id_task` int(11) NOT NULL,
  `date` date NOT NULL,
  `isDone` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `status_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_task_id`),
  UNIQUE KEY `user_tasks_utid_uindex` (`user_task_id`),
  KEY `user_tasks_tasks_tid_fk` (`id_task`),
  KEY `user_tasks_users_uid_fk` (`id_user`),
  KEY `user_tasks_users_uid_fk_2` (`id_user_dep`),
  KEY `user_tasks_users_uid_fk_3` (`created_by`),
  KEY `user_tasks_users_uid_fk_4` (`status_updated_by`),
  CONSTRAINT `user_tasks_tasks_tid_fk` FOREIGN KEY (`id_task`) REFERENCES `tasks` (`task_id`) ON DELETE CASCADE,
  CONSTRAINT `user_tasks_users_uid_fk` FOREIGN KEY (`id_user`) REFERENCES `users` (`user_id`),
  CONSTRAINT `user_tasks_users_uid_fk_2` FOREIGN KEY (`id_user_dep`) REFERENCES `users` (`user_id`),
  CONSTRAINT `user_tasks_users_uid_fk_3` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`),
  CONSTRAINT `user_tasks_users_uid_fk_4` FOREIGN KEY (`status_updated_by`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=592 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(32) NOT NULL,
  `id_user_role` int(11) NOT NULL DEFAULT 3,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_uid_uindex` (`user_id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  KEY `users_user_roles_urid_fk` (`id_user_role`),
  CONSTRAINT `users_user_roles_urid_fk` FOREIGN KEY (`id_user_role`) REFERENCES `user_roles` (`user_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
