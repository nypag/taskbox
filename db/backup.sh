# Small "script" that creates a backup of the database. 
# run backup.sh user password
cd /root/taskbox 
docker-compose exec -T db sh -c 'exec mysqldump --all-databases --add-drop-database -u"$1" -p"$2"' > /mnt/backup/taskbox-$(date +"%d-%m-%y").sql